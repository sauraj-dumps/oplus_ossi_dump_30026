-- val_start_ts,      --obrain_start_ts
-- val_end_ts,        --obrain_end_ts
-- val_wall_start_ts  --wall_start_ts_round

INSERT INTO agg_cpu_app_daily
SELECT
    {val_start_ts} AS start_ts,
    {val_end_ts} AS end_ts,
    {val_wall_start_ts} AS timestamp,
    uid,
    name,
    sum(foreground_duration) AS foreground_duration,
    sum(foreground_energy) AS foreground_energy,
    sum(background_duration) AS background_duration,
    sum(background_energy) AS background_energy,
    sum(screen_on_duration) AS screen_on_duration,
    sum(screen_on_energy) AS screen_on_energy,
    sum(screen_off_duration) AS screen_off_duration,
    sum(screen_off_energy) AS screen_off_energy
FROM agg_cpu_app_hourly
WHERE start_ts >= {val_start_ts} AND end_ts <= {val_end_ts}
GROUP BY uid, name;

INSERT INTO agg_cpu_device_daily
SELECT
    start_ts, end_ts, timestamp,
    sum(foreground_duration) AS foreground_duration,
    sum(foreground_energy) AS foreground_energy,
    sum(background_duration) AS background_duration,
    sum(background_energy) AS background_energy,
    sum(screen_on_duration) AS screen_on_duration,
    sum(screen_on_energy) AS screen_on_energy,
    sum(screen_off_duration) AS screen_off_duration,
    sum(screen_off_energy) AS screen_off_energy
FROM agg_cpu_app_daily
WHERE start_ts >= {val_start_ts} AND end_ts <= {val_end_ts}
GROUP BY start_ts, end_ts, timestamp;

--by_app  upload dcs-------------------------------------------------------------------------
-- obfuscate: Only applications within the range of AID_APP_START to AID_APP_END require encryption
-- #define AID_APP_START 10000;  #define AID_APP_END 19999
SELECT upload_dcs('{upload_dcs_key}', '{log_tag_app}', '{config_id_app}', {version}, '{type}', {agent_type},
                  '{id}', '{start_wall_time}', '{end_wall_time}', '{meta_data}', by_app)
FROM (
    SELECT json_insert(json_set('{{}}', '$.column_key', json(column_key)), '$.row_value', json(row_value)) AS 'by_app'
    FROM (
        SELECT
            json_set('{{}}', '$', json_array('timestamp', 'uid', 'name', 'foreground_duration',
                'foreground_energy', 'background_duration', 'background_energy', 'screen_on_duration',
                'screen_on_energy', 'screen_off_duration', 'screen_off_energy')) AS column_key,
            json_set('{{}}', '$', json_group_array(json_array(timestamp, uid,
                CASE WHEN ((uid >= 10000) AND (uid <= 19999)) THEN obfuscate(name) ELSE name END,
                foreground_duration, foreground_energy, background_duration, background_energy,
                screen_on_duration, screen_on_energy, screen_off_duration, screen_off_energy))) AS row_value
        FROM agg_cpu_app_daily
        WHERE start_ts >= {val_start_ts} AND end_ts <= {val_end_ts}
    )
);

--by_device  upload dcs-------------------------------------------------------------------------
SELECT upload_dcs('{upload_dcs_key}', '{log_tag_device}', '{config_id_device}', {version}, '{type}', {agent_type},
                  '{id}', '{start_wall_time}', '{end_wall_time}', '{meta_data}', by_device)
FROM (
    SELECT json_insert(json_set('{{}}', '$.column_key', json(column_key)), '$.row_value', json(row_value)) AS 'by_device'
    FROM (
        SELECT
            json_set('{{}}', '$', json_array('timestamp', 'foreground_duration',
                'foreground_energy', 'background_duration', 'background_energy', 'screen_on_duration',
                'screen_on_energy', 'screen_off_duration', 'screen_off_energy')) AS column_key,
            json_set('{{}}', '$', json_group_array(json_array(timestamp,
                foreground_duration, foreground_energy, background_duration, background_energy,
                screen_on_duration, screen_on_energy, screen_off_duration, screen_off_energy))) AS row_value
        FROM agg_cpu_device_daily
        WHERE start_ts >= {val_start_ts} AND end_ts <= {val_end_ts}
    )
);

--by_app
--|start_ts     |end_ts       |timesptamp---|----uid-----|----name----|foreground_duration| foreground_energy|background_duration|background_energy|screen_on_duration  |screen_on_energy|screen_off_duration|screen_off_energy
--|1676359917975|1676359958037|1676358000000|1056        |Binder:horae|24067             |630372              |1477               |33888           |25430              |661645             |114                |       2615
--|1676359917975|1676359958037|1676358000000|1006        |Kworker     |27214             |648866              |0                 |0                |27214              |648866             |0                     |       0

--by_device
--|start_ts     |end_ts       |timesptamp---|foreground_duration|foreground_energy|background_duration|background_energy|screen_on_duration|screen_on_energy|screen_off_duration|screen_off_energy
--|1676359917975|1676359958037|1676358000000|24067              |630372              |1477               |33888          |25430           |661645          |114                |       2615
